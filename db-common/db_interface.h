#pragma once

class DB_Interface {
public:
	virtual bool Connect();
	virtual bool Disconnect();
	virtual void CheckConnect();
	virtual void Generate();
	virtual void Insert();
};