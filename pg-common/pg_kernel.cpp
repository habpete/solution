#include "stdafx.h"
#include "pg_kernel.h"
#include <iostream>

using namespace std;

pg_kernel::pg_kernel()
{
}


pg_kernel::~pg_kernel()
{
}

bool pg_kernel::StructConnect(descriptor& inDescriptor) {
	const char* const* keys = new const char* const[5];
	const char* const* values = new const char* const[5];
	pg_kernel::connect = PQconnectdbParams(keys, values, 0);
}

bool pg_kernel::Connect() {
	const char* descriptor = "";
	pg_kernel::connect = PQconnectdb(descriptor);
	if (PQstatus(connect) != PGRES_COMMAND_OK) {
		cout << PQerrorMessage(connect) << endl;
		return false;
	}
	return true;
}

bool pg_kernel::Execute(const char *inQuery) {
	PGresult* res = PQexec(pg_kernel::connect, inQuery);
	if (PQresultStatus(res) != PGRES_COMMAND_OK) {
		cout << PQresultErrorMessage(res) << endl;
		return false;
	}
	return true;
}

void pg_kernel::Disconnect() {
	if (pg_kernel::connect != NULL)
		PQfinish(pg_kernel::connect);
}
