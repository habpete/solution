#pragma once
#include "libpq-fe.h"
typedef pg_kernel::connectionStruct descriptor;
class pg_kernel
{
public:
	pg_kernel();
	~pg_kernel();
	struct connectionStruct {
		const char* server;
		const char* database;
		int port;
		const char* name;
		const char* pwd;
	};
	bool StructConnect(descriptor& inDescriptor);
	bool Connect();
	bool Execute(const char *inQuery);
	void Disconnect();
private:
	PGconn* connect;
};

